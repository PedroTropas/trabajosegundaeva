package clases;

public class CosasDelPrograma {

	// Atributos
	private String nombrePrograma;
	private String tipoPrograma;
	private double precio;
	private String categoria;

	// Constructor
	public CosasDelPrograma(String nombrePrograma) {
		this.nombrePrograma = nombrePrograma;
	}

	// Los getters y Setters
	public String getNombrePrograma() {
		return nombrePrograma;
	}

	public void setNombrePrograma(String nombrePrograma) {
		this.nombrePrograma = nombrePrograma;
	}

	public String getTipoPrograma() {
		return tipoPrograma;
	}

	public void setTipoPrograma(String tipoPrograma) {
		this.tipoPrograma = tipoPrograma;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	// El ToString para imprimir
	@Override
	public String toString() {
		return "Programas en la tienda: Programa=" + nombrePrograma + ", Tipo:" + tipoPrograma + ", Precio:"
				+ precio + ", Categoria:" + categoria ;
	}

}
