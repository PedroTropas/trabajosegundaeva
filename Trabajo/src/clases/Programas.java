package clases;

public class Programas {

	private CosasDelPrograma[] Programillas;

	// Constructor
	public Programas(int maxPrograma) {
	// Asignacion
		this.Programillas = new CosasDelPrograma[maxPrograma];
	}

	// Dar de alta programas
	public void altaPrograma(String nombrePrograma, double precio, String tipoPrograma, String categoria) {
		for (int i = 0; i < Programillas.length; i++) {
			if (Programillas[i] == null) {
				Programillas[i] = new CosasDelPrograma(nombrePrograma);
				Programillas[i].setPrecio(precio);
				Programillas[i].setTipoPrograma(tipoPrograma);
				Programillas[i].setCategoria(categoria);
				break;
			}
		}
	}

	// Buscar un programa
	public CosasDelPrograma buscarPrograma(String nombrePrograma) {
		for (int i = 0; i < Programillas.length; i++) {
			if (Programillas[i] != null) {
				if (Programillas[i].getNombrePrograma().equals(nombrePrograma)) {
					return Programillas[i];
				}
			}
		}
		return null;
	}

	// Eliminar un programa
	public void eliminarPrograma(String nombrePrograma) {
		for (int i = 0; i < Programillas.length; i++) {
			if (Programillas[i] != null) {
				if (Programillas[i].getNombrePrograma().equals(nombrePrograma)) {
					Programillas[i] = null;
				}
			}
		}

	}

	// Listar programas
	public void listarProgramas() {
		for (int i = 0; i < Programillas.length; i++) {
			if (Programillas[i] != null) {
				System.out.println(Programillas[i]);
			}
		}
	}

	// Cambiar nombre programa
	public void cambiarNombrePrograma(String nombrePrograma, String nombreProgramaNuevoNombre) {
		for (int i = 0; i < Programillas.length; i++) {
			if (Programillas[i] != null) {
				if (Programillas[i].getNombrePrograma().equals(nombrePrograma)) {
					Programillas[i].setNombrePrograma(nombreProgramaNuevoNombre);
				}
			}
		}
	}

	// Listar programas por categoria
	public void listarProgramasPorCategoria(String nombrePrograma) {
		for (int i = 0; i < Programillas.length; i++) {
			if (Programillas[i] != null) {
				if (Programillas[i].getCategoria().equals(nombrePrograma)) {
					System.out.println(Programillas[i]);
				}
			}
		}
	}
	
	//Listar programas por precio
	public void listarProgramasPorPrecio(double precio) {
		for (int i = 0; i < Programillas.length; i++) {
			if (Programillas[i] != null) {
				if (Programillas[i].getPrecio()==(precio)) {
					System.out.println(Programillas[i]);
				}
			}
		}
	}

	// Cambiar precio programa
		public void cambiarPrecioPrograma(String nombrePrograma, double precioProgramaNuevoPrecio) {
			for (int i = 0; i < Programillas.length; i++) {
				if (Programillas[i] != null) {
					if (Programillas[i].getNombrePrograma().equals(nombrePrograma)) {
						Programillas[i].setPrecio(precioProgramaNuevoPrecio);
						}
				}
			}
		}
}
