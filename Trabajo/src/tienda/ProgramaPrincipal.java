package tienda;

import javax.swing.JOptionPane;

import clases.Programas;

public class ProgramaPrincipal {

	public static void main(String[] args) {

		System.out.println("******************************");
		System.out.println("Inicializa el programa");
		System.out.println("Necesito saber el tama�o de programas que tendra la base");

		int maxProgramas = Integer.parseInt(
				JOptionPane.showInputDialog("Introduce el numero de programas maximos (Recomendacion poner mas de 5)"));

		Programas miTienda = new Programas(maxProgramas);
		System.out.println("Has elegido el numero maximo de programas:" + maxProgramas);
		System.out.println("******************************");

		// Estos programas estarian ya dentro
		System.out.println("De base la aplicacion ya tiene 4 programas ");
		miTienda.altaPrograma("Affinity Photo", 39.99, "Fotos y v�deos", "Productividad");
		miTienda.altaPrograma("Affinity Designer", 29.99, "Fotos y v�deos", "Productividad");
		miTienda.altaPrograma("Affinity Publisher", 29.99, "Fotos y v�deos", "Productividad");
		miTienda.altaPrograma("Spotify", 0, "Musica", "Entretenimiento");
		////////////////////////////////////
		/*
		 * // Alta de Programa Preguntando String nombreProgramaPreguntando,
		 * categoriaPreguntando, tipoPegruntando; double precioPregruntando;
		 * nombreProgramaPreguntando =
		 * JOptionPane.showInputDialog("Introduce el nombre del Programa");
		 * precioPregruntando =
		 * Double.parseDouble(JOptionPane.showInputDialog("Introduce el precio"));
		 * categoriaPreguntando =
		 * JOptionPane.showInputDialog("Introduce la categoria del programa");
		 * tipoPegruntando =
		 * JOptionPane.showInputDialog("Introduce el tipo de programa");
		 * miTienda.altaPrograma(nombreProgramaPreguntando, precioPregruntando,
		 * tipoPegruntando, categoriaPreguntando); ////////////////////////////////////
		 */
		/*
		 * // Opcion listar programas
		 * System.out.println("******************************");
		 * System.out.println("3. Listar programas"); miTienda.listarProgramas();
		 */
		/*
		 * // Opcion Buscar Programa por un nombre en este caso Spotify
		 * System.out.println("******************************");
		 * System.out.println("4. Listar un Programa por el nombre Spotify");
		 * System.out.println(miTienda.buscarPrograma("Spotify"));
		 */
		/*
		 * // Opcion Eliminar un Programa por un nombre en este caso Spotify
		 * System.out.println("******************************");
		 * System.out.println("5. Eliminar el Programa Spotify");
		 * miTienda.eliminarPrograma("Spotify"); miTienda.listarProgramas();
		 */
		/*
		 * // Almacenar un nuevo programa con sus datos
		 * System.out.println("******************************");
		 * System.out.println("6. Almacenamos un nuevo programa");
		 * miTienda.altaPrograma("Itunes", 0, "Musica", "Entretenimiento");
		 * miTienda.listarProgramas();
		 */
		/*
		 * // Modificar el nombre del programa
		 * System.out.println("******************************");
		 * System.out.println("7. Modificar nombre programa");
		 * miTienda.cambiarNombrePrograma("Itunes", "Apple Music");
		 * miTienda.listarProgramas();
		 */
		/*
		 * // Listar programas por la categoria "Productividad"
		 * System.out.println("******************************");
		 * System.out.println("8. Listar Progrmas por la Categoria Productividad");
		 * miTienda.listarProgramasPorCategoria("Productividad");
		 */
		/*
		 * // Listar programas por su precio "29.99"
		 * System.out.println("******************************");
		 * System.out.println("9. Listar Programas por precio 29.99");
		 * miTienda.listarProgramasPorPrecio(29.99);
		 */
		/*
		 * // Cambiar el precio de un programa sabiendo su nombre
		 * System.out.println("******************************");
		 * System.out.println("10 . Cambiar Precio a programa por su nombre");
		 * miTienda.cambiarPrecioPrograma("Apple Music", 99999);
		 * miTienda.listarProgramas();
		 */

		int opcionMenu;
		do {
			System.out.println("**************************************************************");
			System.out.println("*Opcion 1  - Dar de alta un programa                         *");
			System.out.println("*Opcion 2  - Listar programas                                *");
			System.out.println("*Opcion 3  - Listar el programa Spotify                      *");
			System.out.println("*Opcion 4  - Eliminar el Programa Spotify                    *");
			System.out.println("*Opcion 5  - Dar de alta a Itunes                            *");
			System.out.println("*Opcion 6  - Modificar a Itunes por Apple Music              *");
			System.out.println("*Opcion 7  - Listar programas por la categoria Productividad *");
			System.out.println("*Opcion 8  - Listar programas por su precio 29.99            *");
			System.out.println("*Opcion 9  - Cambiar el precio de Apple Music a 99999        *");
			System.out.println("*Opcion 10 - salir                                           *");
			System.out.println("**************************************************************");
			opcionMenu = Integer
					.parseInt(JOptionPane.showInputDialog("Introduce la opcion del 1 al 10 ----- 10 para salir)"));

			switch (opcionMenu) {
			case 1:
				// Alta de Programa Preguntando
				String nombreProgramaPreguntando, categoriaPreguntando, tipoPegruntando;
				double precioPregruntando;
				nombreProgramaPreguntando = JOptionPane.showInputDialog("Introduce el nombre del Programa");
				precioPregruntando = Double.parseDouble(JOptionPane.showInputDialog("Introduce el precio"));
				categoriaPreguntando = JOptionPane.showInputDialog("Introduce la categoria del programa");
				tipoPegruntando = JOptionPane.showInputDialog("Introduce el tipo de programa");
				miTienda.altaPrograma(nombreProgramaPreguntando, precioPregruntando, tipoPegruntando,
						categoriaPreguntando);
				////////////////////////////////////

				break;

			case 2:
				// Opcion listar programas
				System.out.println("******************************");
				System.out.println("3. Listar programas");
				miTienda.listarProgramas();
				break;
			case 3:
				// Opcion Buscar Programa por un nombre en este caso Spotify
				System.out.println("******************************");
				System.out.println("4. Listar un Programa por el nombre Spotify");
				System.out.println(miTienda.buscarPrograma("Spotify"));

				break;

			case 4:
				// Opcion Eliminar un Programa por un nombre en este caso Spotify
				System.out.println("******************************");
				System.out.println("5. Eliminar el Programa Spotify");
				miTienda.eliminarPrograma("Spotify");
				miTienda.listarProgramas();
				break;


			case 5:

				// Almacenar un nuevo programa con sus datos
				System.out.println("******************************");
				System.out.println("6. Almacenamos un nuevo programa");
				miTienda.altaPrograma("Itunes", 0, "Musica", "Entretenimiento");
				miTienda.listarProgramas();

				break;
			case 6:

				// Modificar el nombre del programa
				System.out.println("******************************");
				System.out.println("7. Modificar nombre programa");
				miTienda.cambiarNombrePrograma("Itunes", "Apple Music");
				miTienda.listarProgramas();

				break;
			case 7:

				// Listar programas por la categoria "Productividad"
				System.out.println("******************************");
				System.out.println("8. Listar Progrmas por la Categoria Productividad");
				miTienda.listarProgramasPorCategoria("Productividad");

				break;

			case 8:

				// Listar programas por su precio "29.99"
				System.out.println("******************************");
				System.out.println("9. Listar Programas por precio 29.99");
				miTienda.listarProgramasPorPrecio(29.99);

				break;

			case 9:

				// Cambiar el precio de un programa sabiendo su nombre
				System.out.println("******************************");
				System.out.println("10 . Cambiar Precio a programa por su nombre");
				miTienda.cambiarPrecioPrograma("Apple Music", 99999);
				miTienda.listarProgramas();

				break;

			case 10:
				System.out.println("Has elegido salir");
				System.out.println("Programa Creado Por: Pedro Tropas�");
				break;

			default:
				break;
			}
		} while (opcionMenu != 10);

	}

}
